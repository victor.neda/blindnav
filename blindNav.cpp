#include <deptrum/deptrum.h>
#include <fstream>
#include <iostream>
#include <pigpio.h>
#include <math.h>
#include "base.h"

#define MAX_ELEMENTS 307200

using namespace deptrum;
using namespace stellar;

void notify_Callback();
void meassure_Callback();

typedef struct
{
   int x;
   int y;
   float depth;
}TofToPlacingData;

TofToPlacingData temp_buffer_of_TofData[MAX_ELEMENTS];

int value = 0;
int point_len;
int pulse= 0;

float prev_min;

bool LEFT = false;
bool CENTER = false;
bool RIGHT = false;
bool U_1_M_WARNING = false;

int volume_matrix[3][4];


float frequencyOfHapticNotifications(float depth)
{
   float freq = 10;

   if((depth>=1) && (depth<2))
   { 
     freq = (2-depth)*500;
     freq=freq/100;
     freq=freq*100;
   }
   else if(fabs(depth - prev_min) >= 0.1)
   {
     freq = 10;
   }

   return freq;
}

float mmTomConversion(float depth_mm)
{
    float depth_m =depth_mm/100;
    depth_m = (int)depth_m; 
    depth_m = depth_m/10;
  
    return depth_m;
}

float getMinDepth(float depth_val, float prev_depth_val)
{
   float new_min_depth = prev_depth_val;
   
   if(depth_val < new_min_depth)
   {
     new_min_depth = depth_val;
   }
   
   return new_min_depth;
}

float *segmentationOfFrame()
{
     static float zoneMean[12];
     float z1_sum = 0, z2_sum = 0, z3_sum = 0, z4_sum = 0, z5_sum = 0, z6_sum = 0, z7_sum = 0, z8_sum = 0, z9_sum = 0, z10_sum = 0, z11_sum = 0, z12_sum = 0 ;
     int z1_cnt = 0, z2_cnt = 0, z3_cnt = 0, z4_cnt = 0, z5_cnt = 0, z6_cnt = 0, z7_cnt = 0, z8_cnt = 0, z9_cnt = 0, z10_cnt = 0, z11_cnt = 0, z12_cnt = 0 ;
     
     int i;
     for(i=0; i<MAX_ELEMENTS; i++)
     {
        int x = temp_buffer_of_TofData[i].x;
        int y = temp_buffer_of_TofData[i].y;
        float depth = temp_buffer_of_TofData[i].depth;
        
        if(x/160 == 0)
        {
          if((y >= 0) && (y<160))
          {
            z1_sum+=depth;
            z1_cnt++;
            zoneMean[0] = (z1_sum/z1_cnt);
          }
          else if((y >= 160) && (y<320))
          {
            z2_sum+=depth;
            z2_cnt++;
            zoneMean[1] = (z2_sum/z2_cnt);
          }
          else if((y >= 320) && (y<480))
          {
            z3_sum+=depth;
            z3_cnt++;
            zoneMean[2] = (z3_sum/z3_cnt);
          }
          else if((y >= 480) && (y<640))
          {
            z4_sum+=depth;
            z4_cnt++;
            zoneMean[3] = (z4_sum/z4_cnt);
          }
        }
        else if(x/160 == 1)
        {
          if((y >= 0) && (y<160))
          {
            z5_sum+=depth;
            z5_cnt++;
            zoneMean[4] = (z5_sum/z5_cnt);
          }
          else if((y >= 160) && (y<320))
          {
            z6_sum+=depth;
            z6_cnt++;
            zoneMean[5] = (z6_sum/z6_cnt);
          }
          else if((y >= 320) && (y<480))
          {
            z7_sum+=depth;
            z7_cnt++;
            zoneMean[6] = (z7_sum/z7_cnt);
          }
          else if((y >= 480) && (y<640))
          {
            z8_sum+=depth;
            z8_cnt++;
            zoneMean[7] = (z8_sum/z8_cnt);
          }
          
        }
        else if(x/160 == 2)
        {
          if((y >= 0) && (y<160))
          {
            z9_sum+=depth;
            z9_cnt++;
            zoneMean[8] = (z9_sum/z9_cnt);
          }
          else if((y >= 160) && (y<320))
          {
            z10_sum+=depth;
            z10_cnt++;
            zoneMean[9] = (z10_sum/z10_cnt);
          }
          else if((y >= 320) && (y<480))
          {
            z11_sum+=depth;
            z11_cnt++;
            zoneMean[10] = (z11_sum/z11_cnt);
          }
          else if((y >= 480) && (y<640))
          {
            z12_sum+=depth;
            z12_cnt++;
            zoneMean[11] = (z12_sum/z12_cnt);
          }
          
        }         
     }
     
     return zoneMean;
}

void initVolumeMatrix()
{
  for(int row=0; row<3; row++)
  {
     for(int col=0; col<4; col++)
     {
       volume_matrix[row][col] = 0;
     }
  }
  
}


void computeVolumeOfObstacle(float min_depth)
{
  int sizePercentage[12];
  int z1_cnt = 0, z2_cnt = 0, z3_cnt = 0, z4_cnt = 0, z5_cnt = 0, z6_cnt = 0, z7_cnt = 0, z8_cnt = 0, z9_cnt = 0, z10_cnt = 0, z11_cnt = 0, z12_cnt = 0 ;
  
  if(min_depth < 2)
  {
     int i;
     for(i=0; i<MAX_ELEMENTS; i++)
     {
        int x = temp_buffer_of_TofData[i].x;
        int y = temp_buffer_of_TofData[i].y;
        float depth = temp_buffer_of_TofData[i].depth;
        
        if(mmTomConversion(depth) == min_depth){
        if(x/160 == 0)
        {
          if((y >= 0) && (y<160))
          {
            z1_cnt++;
            sizePercentage[0] = z1_cnt;
          }
          else if((y >= 160) && (y<320))
          {
            z2_cnt++;
            sizePercentage[1] = z2_cnt;
          }
          else if((y >= 320) && (y<480))
          {
            z3_cnt++;
            sizePercentage[2] = z3_cnt;
          }
          else if((y >= 480) && (y<640))
          {
            z4_cnt++;
            sizePercentage[3] = z4_cnt;
          }
        }
        else if(x/160 == 1)
        {
          if((y >= 0) && (y<160))
          {
            z5_cnt++;
            sizePercentage[4] = z5_cnt;
          }
          else if((y >= 160) && (y<320))
          {
            z6_cnt++;
            sizePercentage[5] = z6_cnt;
          }
          else if((y >= 320) && (y<480))
          {
            z7_cnt++;
            sizePercentage[6] = z7_cnt;
          }
          else if((y >= 480) && (y<640))
          {
            z8_cnt++;
            sizePercentage[7] = z8_cnt;
          }
          
        }
        else if(x/160 == 2)
        {
          if((y >= 0) && (y<160))
          {
            z9_cnt++;
            sizePercentage[8] = z9_cnt;
          }
          else if((y >= 160) && (y<320))
          {
            z10_cnt++;
            sizePercentage[9] = z10_cnt;
          }
          else if((y >= 320) && (y<480))
          {
            z11_cnt++;
            sizePercentage[10] = z11_cnt;
          }
          else if((y >= 480) && (y<640))
          {
            z12_cnt++;
            sizePercentage[11] = z12_cnt;
          }
          
        } 
      }        
  }  
  
  int maxPercentage = 0;
  int row;
  int col;
  
  for(int i=0; i<12;i++)
  {
    std::cout<<sizePercentage[i]<<std::endl;
    if(sizePercentage[i] > maxPercentage)
    {
      maxPercentage = sizePercentage[i];
    }
  }
  
  for(int i=0; i<12;i++)
  {
    row = i/4;
    col = i%4;
    if(abs(sizePercentage[i] - maxPercentage) <= 1500)
    {
      volume_matrix[row][col] = 1;
    }
  }
  
  
}
}

void detectOccupiedVolume(int &left_cnt, int &center_left_cnt, int &center_right_cnt, int &right_cnt)
{
  for(int row=0; row<3; row++)
  {
     for(int col=0; col<4; col++)
     {
       std::cout<<volume_matrix[row][col]<<" ";
       if(volume_matrix[row][col] == 1)
       {
         if(col == 0)
           left_cnt++;
         else if(col == 1) 
           center_left_cnt++;
         else if(col == 2) 
           center_right_cnt++;
        else if(col == 3)
           right_cnt++;
       }
     }
     std::cout<<std::endl;
  }
}

void obstaclePositionDtection(float *depth_arr, float min_depth)
{
  LEFT = false;
  CENTER = false;
  RIGHT = false;
  U_1_M_WARNING = false;
    
  int lcnt = 0;
  int clcnt = 0;
  int crcnt = 0;
  int rcnt = 0;
  
    computeVolumeOfObstacle(min_depth);
    detectOccupiedVolume(lcnt,clcnt, crcnt, rcnt);
    
    if(min_depth < 2){
    
    if(min_depth < 1)
    {
      U_1_M_WARNING = true;
    }
    
    if((lcnt == clcnt) && (clcnt == crcnt) && (crcnt == rcnt))
    {
      CENTER = true;
    }
    
    else if((lcnt == clcnt) && (clcnt == crcnt) && (rcnt == 0))
    {
      CENTER = true;
      
    }
    else if((lcnt == 0) && (clcnt == crcnt) && (crcnt == rcnt))
    {
      CENTER = true;
      
    }
    
    else if((lcnt < rcnt) && (clcnt < rcnt) && (crcnt <=rcnt))
    {
      RIGHT = true;
      
    }
    else if((lcnt < crcnt) && (clcnt < crcnt) && (rcnt <=crcnt))
    {
      RIGHT = true;
      
    }
    
    else if((lcnt >= clcnt) && (lcnt > crcnt) && (lcnt > rcnt))
    {
      LEFT = true;
      
    }
    else if((lcnt >= clcnt) && (clcnt > crcnt) && (clcnt > rcnt))
    {
      LEFT = true;
      
    }
    else
    {
       CENTER = true;
    }
    
    
  }
  
}

void notify_Callback()
{
    pulse^=0xFF;
    
    if((U_1_M_WARNING == true) && (CENTER ==true))
    {
      gpioPWM(12,255);
      gpioPWM(13,255);
    }
    else if((U_1_M_WARNING == true) && (LEFT ==true))
    {
      gpioPWM(12,0);
      gpioPWM(13,255);
      std::cout<<"HERE!"<<std::endl;
    }
    else if((U_1_M_WARNING == true) && (RIGHT ==true))
    {
      gpioPWM(12,255);
      gpioPWM(13,0);
    }
    else if((U_1_M_WARNING == false) && (CENTER == true))
    {
      gpioPWM(12,pulse);
      gpioPWM(13,pulse);
    }
    else if((U_1_M_WARNING == false) &&(CENTER == false) && (LEFT == false) && (RIGHT == false))
    {
      gpioPWM(12,0);
      gpioPWM(13,0);
    }
    else if((U_1_M_WARNING == false) && (LEFT == true))
    {
      gpioPWM(12,0);
      gpioPWM(13,pulse);
    }
    else if((U_1_M_WARNING == false) && (RIGHT == true))
    {
      gpioPWM(12,pulse);
      gpioPWM(13,0);
      
    }
    

}

void meassure_Callback(void)
{
   float *depth_buffer;
   float notifFreq;
   
   depth_buffer = segmentationOfFrame();
   
   float min_depth = getMinDepth(mmTomConversion(depth_buffer[0]), 6);
   
   for(int i=1;i<12;i++)
  {
    min_depth = getMinDepth(mmTomConversion(depth_buffer[i]), min_depth);
    
  }
  
  notifFreq = frequencyOfHapticNotifications(min_depth);
  gpioSetTimerFunc(1, notifFreq, notify_Callback);
  
  initVolumeMatrix();
  obstaclePositionDtection(depth_buffer, min_depth);
   
  prev_min = min_depth;
  

}

int main() {
  
   if(gpioInitialise()<0) return 0;
   gpioSetMode(12, PI_OUTPUT);
   gpioSetMode(13, PI_OUTPUT);

  std::vector<DeviceInformation> device_list;
  int ret = Device::GetDeviceList(device_list);
  CHECK_SDK_RETURN_VALUE(ret);

  CHECK_DEVICE_COUNT(device_list.size());

  Device* device = nullptr;
  Device::Create(&device, device_list[0]);
  CHECK_DEVICE_VALID(device);

  FrameMode ir_mode, rgb_mode, depth_mode;
  ir_mode = FrameMode::kRes640x480Depth16Bit;
  rgb_mode = FrameMode::kInvalid;
  depth_mode = FrameMode::kInvalid;

  ret = device->Open();
  CHECK_SDK_RETURN_VALUE(ret);

  ret = device->SetMode(ir_mode, rgb_mode, depth_mode);
  CHECK_SDK_RETURN_VALUE(ret);

  Stream* point_cloud_stream = nullptr;
  ret = device->CreateStream(&point_cloud_stream, StreamType::kPointDense);
  CHECK_SDK_RETURN_VALUE(ret);

  Frame point_cloud_frame;
  point_cloud_stream->AllocateFrame(point_cloud_frame);

  ret = point_cloud_stream->Start();
  CHECK_SDK_RETURN_VALUE(ret);

  int cnt = 1;
  FrameRateHelper frame_rate_helper;

  gpioSetTimerFunc(0, 100, meassure_Callback);

  while (1) {
    
    ret = point_cloud_stream->GetFrame(point_cloud_frame, 1000);
    CHECK_GET_FRAMES(ret);
    
    if (cnt % 10 == 0) {
     
      PointXyzRgbIr<float>* data_pointcloud = static_cast<PointXyzRgbIr<float>*>(point_cloud_frame.data);
      point_len = point_cloud_frame.size / point_cloud_frame.bytes_per_pixel;
      
      for (int n = 0; n < MAX_ELEMENTS; n++) 
      {
          temp_buffer_of_TofData[n].x = n/640;
          temp_buffer_of_TofData[n].y = n%640;
          if(n < point_len)
          {           
              temp_buffer_of_TofData[n].depth = data_pointcloud[n].z;
          }
          else
          {
              temp_buffer_of_TofData[n].depth = temp_buffer_of_TofData[n-640].depth;
          }
                  
      }
      
      cnt=1;
     
    }
    cnt++;
    
  }

  ret = point_cloud_stream->Stop();
  CHECK_SDK_RETURN_VALUE(ret);
  point_cloud_stream->ReleaseFrame(point_cloud_frame);

  ret = device->DestroyStream(&point_cloud_stream);
  CHECK_SDK_RETURN_VALUE(ret);

  ret = device->Close();
  CHECK_SDK_RETURN_VALUE(ret);
  Device::Destroy(&device);

  gpioTerminate();

  return 0;
}
